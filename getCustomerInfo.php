<?php

/**
* this sample code to get customer info data using Omas API
* Get customer information data is use GET method to send request
*
* @param speedyNumber (AccountID)
* @return object
* 
**/

require_once('sendRequest.php');

// -------------------- Send customer info request ------------------- //
$speedyNumber = '555577778888';
$result = getCustomerInfo($speedyNumber);

if($result->statusCode != 0) {

	// Your code to handle failed result
	echo $result->statusMessage;

} else {

	// your code to handle Successed result
?>

	<h2>Customer Info</h2>
	<p>
		<ul>
			<li>ID: <?php echo $result->customerId; ?></li>
			<li>Speedy: <?php echo $result->customerSpeedy; ?></li>
			<li>Nama: <?php echo $result->customerName; ?></li>
			<li>Alamat: <?php echo $result->customerAddress; ?></li>
			<li>Email: <?php echo $result->customerEmail; ?></li>
			<li>Telp: <?php echo $result->customerTelp; ?></li>
			<li>Kuota device: <?php echo $result->customerQuota; ?></li>
			<li>paket: <?php echo $result->package->name; ?></li>
			<li>Perangkat yang pernah dibeli:
				<ul>
				<?php
					foreach($result->devices as $device) {
					?>
						<li>Device: <?php echo $device->deviceName; ?></li>
						<li>Qty: <?php echo $device->deviceQty; ?></li>
					<?php	
					}
				?>
				</ul>
			</li>
		</ul>
	</p>

<?php	

}
