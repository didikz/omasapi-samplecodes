<?php

/**
* this sample code to send payment status of an order using Omas API
* payment status is use PUT method to send request
* 
* @return object
* 
**/

require_once('sendRequest.php');

$invoice       = '30';
$invoiceStatus = array('invoiceStatus' => 'Paid');

$result = paymentStatus($invoice, $invoiceStatus);

if($result->statusCode != 0 ) {

	// your code to handle failed update
	echo $result->statusMessage;

} else {

	// your code to handle Successed update status payment
	echo $result->statusMessage;

}
