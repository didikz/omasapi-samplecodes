<?php

/**
* this sample code to get detail device data using Omas API
* Get detail device data is use GET method to send request
*
* @param deviceCode 
* @return object
* 
**/

require_once('sendRequest.php');

if(isset($_GET['id'])) {

	$deviceCode = $_GET['id'];

} else {

	$deviceCode = 1; // default

}

// -------------------- Send detail data device request ------------------- //

$result = getDetailDevice($deviceCode);

if ($result->statusCode != 0 ) {

	// Your code to handle failed result
	echo $result->statusMessage;

} else {
	
	// Your code to handle successed result
?>

	<h3><?php echo $result->deviceDetails->deviceName; ?></h3>
	<p>
		<ul>
			<li>Stock: <?php echo $result->deviceDetails->deviceStock; ?></li>
			<li>Harga: Rp. <?php echo number_format($result->deviceDetails->devicePrice, 2, ',', '.'); ?></li>
			<li>Deskripsi dan Spesifikasi: <?php echo $result->deviceDetails->deviceDescription; ?></li>
			<li><img src="<?php echo $result->deviceDetails->deviceImgUrl; ?>" width="200" height="200"/></li>
		</ul>
	</p>
<?php	

}

?>
