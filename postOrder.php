<?php

/**
* this sample code to send order device data using Omas API
* order device is use POST method to send request
* 
* @return object
* 
**/
require_once('sendRequest.php');

// ------- Build data order to json format ------------ //
$dataArr = array(
				'AccountID' => '1200087631',
				'devices'   => array(
									array(
										'device_id' => 1,
										'qty'		=> 1
									),
									array(
										'device_id' => 2,
										'qty'		=> 1
									)
								)
			);

$dataOrder = json_encode($dataArr);

// ------------------ Send order request ----------------- //
$result = postOrder($dataOrder);

if($result->statusCode != 0 ) {

	// your code to handle failed order
	echo $result->statusMessage;

} else {

	// your code to handle Successed order
	echo ' Order success, invoice: '.$result->invoice;

}
