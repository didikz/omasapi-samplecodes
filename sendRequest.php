<?php

/**
*  This is sample codes of request function to Omas API
*  All function is using curl (recommended)
*
*  @return object
*
**/

function getAllDevices()
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

    // ------ Send GET all devices request using curl function ------- //
    $url = 'http://localhost/omasapi/public/devices?apikey='.$apikey.'&apisecret='.$apisecret;
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    $decode = json_decode($data);
    
    return $decode;

}

function getDetailDevice($deviceCode)
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

    // ------ Send GET all devices request using curl function ------- //
    $url = 'http://localhost/omasapi/public/device/'.$deviceCode.'?apikey='.$apikey.'&apisecret='.$apisecret;
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    $decode = json_decode($data);
    
    return $decode;

}

function getCustomerInfo($speedyNumber)
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

    // ------ Send GET customer info request using curl function ------- //
    $url = 'http://localhost/omasapi/public/customer/info/'.$speedyNumber.'?apikey='.$apikey.'&apisecret='.$apisecret;
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    $decode = json_decode($data);
    
    return $decode;

}

function postOrder($dataOrder)
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

	// ------------- Send post data order request using curl function ----------- //
	$url = 'http://localhost/omasapi/public/order?apikey='.$apikey.'&apisecret='.$apisecret;
	$ch = curl_init($url);                           
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                        
	curl_setopt($ch, CURLOPT_POSTFIELDS, $dataOrder);                   
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                       
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                       
	    'Content-Type: application/json',                                                  
	    'Content-Length: ' . strlen($dataOrder))                                        
	);
	$result = curl_exec($ch);
	curl_close($ch);
	$decode = json_decode($result);
    
    return $decode;
}

function authentication($dataAuth)
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

	// -------- Send post customer authentication data using curl function --------------//
	$url = 'http://localhost/omasapi/public/customer/auth?apikey='.$apikey.'&apisecret='.$apisecret;
	$ch = curl_init($url);                           
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POST, 2);                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dataAuth));                   
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                       
	
	$result = curl_exec($ch);
	curl_close($ch);
	$decode = json_decode($result);
    
    return $decode;
}

function paymentStatus($invoice, $invoiceStatus)
{
	// ------ Define your API key and API Secret here ----- //
	$apikey    = 'test';
	$apisecret = 'test';

	// ------------- Send put payment status request using curl function ----------- //
	$url = 'http://localhost/omasapi/public/order/'.$invoice.'?apikey='.$apikey.'&apisecret='.$apisecret;

	$ch = curl_init($url);                           
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                    
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($invoiceStatus));                   
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	$decode = json_decode($result);
    
    return $decode;

}
