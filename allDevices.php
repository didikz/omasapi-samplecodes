<?php

/**
* this sample code to get all devices data using Omas API
* Get all device data is use GET method to send request
*
* @return object
* 
**/

require_once('sendRequest.php');

// -------------------- Send get all data devices request ------------------- //

$result = getAllDevices();
if($result->statusCode != 0) {

	// Your code to handle failed result
	echo $result->statusMessage;

} else {

	// your code to handle successed result of all data devices
	?>
		<h2>All Devices</h2>
	<?php

	foreach ($result->devices as $device) {
	
	?>
	<ul>
		<li>
			<a href="detailDevice.php?id=<?php echo $device->deviceId; ?>"><?php echo $device->deviceName; ?></a>
			<ul>
				<li>Stock: <?php echo $device->deviceStock; ?></li>
				<li>Harga: Rp. <?php echo number_format($device->devicePrice, 2, ',', '.'); ?></li>
				<li><img src="<?php echo $device->deviceImgUrl; ?>" width="200" height="200"/></li>
			</ul>
		</li>
	</ul>
	<?php
	}
}

?>
