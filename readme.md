#Omas API Sample Codes

This is sample codes for sample request to Usmarthome Order Management Tools (OMAS) API and is built with PHP code.

##Official Documenation
Developer should refer [API Documentation](https://bitbucket.org/didikz/omasapi/wiki/Home) to see full documentation of OMAS API. Some explanation of this sample codes is available at [Sample Codes Wiki](https://bitbucket.org/didikz/omasapi/wiki/Sample%20Code).

**Copyright by PT Telekomunikasi Indonesia**